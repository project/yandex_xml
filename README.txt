--- SUMMARY ---

This module integrates your site with Yandex XML search service.

--- INSTALLATION ---

To set up and start using Yandex.XML in Drupal site, follow these steps:
  1. Ensure your site is indexed by Yandex search engine.
  2. Familiarize yourself with license agreements.
  3. Register the IP address that you plan to send search requests from.
  4. Install the module as usual, see http://drupal.org/node/895232 for further information.
  5. Navigate to  Administration » Configuration » Search and metadata » Yandex XML settings and setup service authorization.
  6. Open yandex/search page and make sure that the search works well.
  
  See more information on project page: http://drupal.org/project/yandex_xml  

--- CONTACT ---

Current maintainer:
Ch - http://drupal.org/user/556138
